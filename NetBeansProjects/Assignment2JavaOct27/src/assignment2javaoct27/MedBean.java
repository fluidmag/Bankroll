/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2javaoct27;

import java.sql.Timestamp;

/**
 *
 * @author cha_xi
 */
public class MedBean {
    private int ID;
    private int patientID;
    private Timestamp dateOfMed;
    private String med;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.ID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MedBean other = (MedBean) obj;
        if (this.ID != other.ID) {
            return false;
        }
        return true;
    }
    private double unitCost;
    private double units;

    public MedBean(int ID, int patientID, Timestamp dateOfMed, String med, double unitCost, double units) {
        this.ID = ID;
        this.patientID = patientID;
        this.dateOfMed = dateOfMed;
        this.med = med;
        this.unitCost = unitCost;
        this.units = units;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public Timestamp getDateOfMed() {
        return dateOfMed;
    }

    public void setDateOfMed(Timestamp dateOfMed) {
        this.dateOfMed = dateOfMed;
    }

    public String getMed() {
        return med;
    }

    public void setMed(String med) {
        this.med = med;
    }

    public double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(double unitCost) {
        this.unitCost = unitCost;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    @Override
    public String toString() {
        return "MedBean{" + "ID=" + ID + ", patientID=" + patientID + ", dateOfMed=" + dateOfMed + ", med=" + med + ", unitCost=" + unitCost + ", units=" + units + '}';
    }
    
    
}
