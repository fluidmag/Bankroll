/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2javaoct27;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

public class PatientDAOImpl implements PatientDAO {
//	private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private final Logger log = Logger.getLogger(this.getClass().getName());

    // This information should be coming from a Properties file
    private final String url = "jdbc:mysql://localhost:3306/HOSPITALDB";
    private final String user = "TheUser";
    private final String password = "pancake";

    /**
     * deleteTemplate used as a template for delete rows in tables and can be
     * applied to delete rows in different tables
     *
     * @param id
     * @param tableName
     * @param itemName
     * @return
     * @throws SQLException
     */
	private Patient createPatient(ResultSet resultSet) throws SQLException {
		Patient p = new Patient();
		p.setPatientID(resultSet.getInt("PATIENTID"));
		p.setLastName(resultSet.getString("LASTNAME"));
		p.setFirstName(resultSet.getString("FIRSTNAME"));
		p.setDiagnosis(resultSet.getString("DIAGNOSIS"));
		p.setAdmissionDate(resultSet.getTimestamp("ADMISSIONDATE"));
		p.setReleaseDate(resultSet.getTimestamp("RELEASEDATE"));
                
		return p;
	}    
    private int deleteTemplate(int id, String tableName, String itemName) throws SQLException {
        int result = 0;
        String deleteQuery = "DELETE FROM " + tableName + " WHERE " + itemName + " = ?";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    @Override
    public int deleteMed(int id) throws SQLException {
        return (deleteTemplate(id, "MEDICATION", "ID"));
    }

    @Override
    public int deleteSur(int id) throws SQLException {
        return (deleteTemplate(id, "SURGICAL", "ID"));
    }

    @Override
    public int deleteInp(int id) throws SQLException {
        return (deleteTemplate(id, "INPATIENT", "ID"));
    }

    public int insertMed(MedBean b) throws SQLException {
        int result = 0;
        String createQuery = "INSERT INTO " + "MEDICATION"
                + " (ID, PATIENTID, DATEOFMED, MED, UNITCOST, UNITS) VALUES (?,?,?,?,?,?)";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(createQuery);) {
            ps.setString(1, Integer.toString(b.getID()));
            ps.setString(2, Integer.toString(b.getPatientID()));
            ps.setString(3, b.getDateOfMed().toString());
            ps.setString(4, b.getMed());
            ps.setString(5, Double.toString(b.getUnitCost()));
            ps.setString(6, Double.toString(b.getUnits()));

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
        return result;
    }

    @Override
    public int insertSur(SurgicalBean b) throws SQLException {
        int result = 0;
        String createQuery = "INSERT INTO " + "SURGICAL"
                + " (ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES) VALUES (?,?,?,?,?,?,?)";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(createQuery);) {
            ps.setString(1, Integer.toString(b.getId()));
            ps.setString(2, Integer.toString(b.getPatientId()));
            ps.setString(3, b.getDateOfSurgery().toString());
            ps.setString(4, b.getSurgery());
            ps.setString(5, Double.toString(b.getRoomFee()));
            ps.setString(6, Double.toString(b.getSurgeonFee()));
            ps.setString(7, Double.toString(b.getSupplies()));

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
        return result;
    }

    @Override
    public int insertInp(InPatientBean b) throws SQLException {
        int result = 0;
        String createQuery = "INSERT INTO " + "SURGICAL"
                + " (ID, PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES) VALUES (?,?,?,?,?,?,?)";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(createQuery);) {
            ps.setString(1, Integer.toString(b.getId()));
            ps.setString(2, Integer.toString(b.getPatientId()));
            ps.setString(3, b.getDateOfStay().toString());
            ps.setString(4, b.getRoomNumber());
            ps.setString(5, Double.toString(b.getDailyRate()));
            ps.setString(6, Double.toString(b.getSupplies()));
            ps.setString(7, Double.toString(b.getServices()));

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
        return result;
    }

    @Override
    public int updateMed(MedBean b) throws SQLException {
        int result = 0;

        String updateQuery = "UPDATE MEDICATION SET PATIENTID=?, DATEOFMED=?, MED=?, UNITCOST=?, UNITS=? WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, Integer.toString(b.getPatientID()));
            ps.setString(2, b.getDateOfMed().toString());
            ps.setString(3, b.getMed());
            ps.setString(4, Double.toString(b.getUnitCost()));
            ps.setString(5, Double.toString(b.getUnits()));
            ps.setString(6, Integer.toString(b.getID()));

            result = ps.executeUpdate();
        }
        log.info("# of records updated : " + result);
        return result;
    }

    @Override
    public String dateSur(SurgicalBean b) throws SQLException {
        String result = "";
        String strQuery = "SELECT DATEOFSURGERY FROM SURGICAL WHERE ID = ?";
        try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareCall(strQuery);
                ){
            ps.setString(1, Integer.toString(b.getId()));            
            ResultSet rs = ps.executeQuery();
            result = rs.getTimestamp("DATEOFSURGERY").toString();
        }
        return result;
    }

    @Override
    public int updateInp(InPatientBean b) throws SQLException {
        int result = 0;

        String updateQuery = "UPDATE INPATIENT SET PATIENTID=?, DATEOFSTAY=?, ROOMNUMBER=?, DAILYRATE=?, SUPPLIES=?, SERVICES=? WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, Integer.toString(b.getPatientId()));
            ps.setString(2, b.getDateOfStay().toString());
            ps.setString(3, b.getRoomNumber());
            ps.setString(4, Double.toString(b.getDailyRate()));
            ps.setString(5, Double.toString(b.getSupplies()));
            ps.setString(6, Double.toString(b.getServices()));
            ps.setString(7, Integer.toString(b.getId()));

            result = ps.executeUpdate();
        }
        log.info("# of records updated : " + result);
        return result;
    }

    @Override
    public Patient findById(int key) throws SQLException{
        Patient p = new Patient();
        String strQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, "
                + "ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = ?";
        try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement ps = connection.prepareStatement(strQuery);){
            ps.setString(1, Integer.toString(key));
            try (ResultSet resultSet = ps.executeQuery()) {
				while (resultSet.next()) {
					p = createPatient(resultSet);
				}
			}
        }
        return p;
    }

    @Override
    public ArrayList<Patient> findAll() throws SQLException{
        ArrayList<Patient> pList = new ArrayList<>();
  		String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT";
		// Using try with resources
		// This ensures that the objects in the parenthesis () will be closed
		// when block ends. In this case the Connection, PreparedStatement and
		// the ResultSet will all be closed.
		try (Connection connection = DriverManager.getConnection(url, user, password);
				// must use PreparedStatements to guard against SQL
				// Injection
				PreparedStatement pStatement = connection.prepareStatement(selectQuery);
				ResultSet resultSet = pStatement.executeQuery()) {
			while (resultSet.next()) {
				pList.add(createPatient(resultSet));
			}
		}
		log.info("# of records found : " + pList.size());
		return pList;
    }

    @Override
    public int updatePatient(Patient p) throws SQLException {
        int result = 0;

        String updateQuery = "UPDATE PATIENT SET LASTNAME=?, FIRSTNAME=?, DIAGNOSIS=?, ADMISSIONDATE=?, RELEASEDATE=? WHERE PATIENTID = ?";

        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {

            ps.setString(1, p.getLastName());
            ps.setString(2, p.getFirstName());
            ps.setString(3, p.getDiagnosis());
            ps.setString(4, p.getAdmissionDate().toString());
            ps.setString(5, p.getReleaseDate().toString());
            ps.setString(6, Integer.toString(p.getPatientID()));

            result = ps.executeUpdate();
        }
        log.info("# of records updated : " + result);
        return result;
    }

    @Override
    public int insertPatient(Patient p)throws SQLException{
        int result = 0;
        String createQuery = "INSERT INTO " + "PATIENT"
                + " (PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE) VALUES (?,?,?,?,?,?)";
        // Connection is only open for the operation and then immediately closed
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(createQuery);) {
            ps.setString(1, Integer.toString(p.getPatientID()));
            ps.setString(2, p.getLastName());
            ps.setString(3, p.getFirstName());
            ps.setString(4, p.getDiagnosis());
            ps.setString(5, p.getAdmissionDate().toString());
            ps.setString(6, p.getReleaseDate().toString());

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
        return result;  
    }

    @Override
    public int deletePatient(Patient p) throws SQLException{
      return deleteTemplate(p.getPatientID(), "PATIENT", "PATIENTID"); 
    }

}
