/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jraps;

import GamePassLine.PassLineImpl;
import java.util.Scanner;

/**
 * Execute Pass Line Gamble
 * @author cha_xi
 */
public class JRapsPassLine extends JRaps {
    private final PassLineImpl gamePassLine;

    public JRapsPassLine(PassLineImpl gamePassLine) {
        super();
        this.gamePassLine = gamePassLine;
        
    }
    public JRapsPassLine() {
        super();
        this.gamePassLine = new PassLineImpl();
    }

    @Override
    public double changeMoney() {
        switch (gamePassLine.getResult()) {
            case -1:
                return -super.getBet();
            case 1:
                return super.getBet();
            case 0:
                return 0;
            default:
                return 0;
        }
    }

    public void perform() {
        double depositMoney = 0;
        double betOnce = 0;
        // ask customer to make a deposit;
        System.out.println("make deposit first!");
        depositMoney = super.inputPositiveNumber();
        //finish deposit and check total residual
        System.out.println("after deposit, your total money is "+
                this.depositAccount(depositMoney));    
        //ask customer to input a bet
        System.out.println("make a bet now");
        do{betOnce = super.inputPositiveNumber();}
        while(!super.setBet(betOnce));
        
        do{
//      gambling begin
        //rolling dice
        gamePassLine.GameRun();
        //check dice numbers
        System.out.println(gamePassLine.toString());
        //change bankroll after one turn
        System.out.println("now you have "+
                super.changeAccount(this.changeMoney()));
        }while(this.changeMoney()==0);
    }
    /**
     * run pass line gamble
     * @param args 
     */
    public static void main(String[] args) {
        JRapsPassLine play = new JRapsPassLine();
        
        play.perform();
                
    }

}
