/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jraps;

import GameFieldBet.FieldBetImpl;

/**
 * Execute Field Bet Gamble
 * @author cha_xi
 */
public class JRapsFieldBet extends JRaps {
    private final FieldBetImpl gameFieldBet;
    
    public JRapsFieldBet(){
        super();
        gameFieldBet = new FieldBetImpl();
    }
    
    @Override
    public double changeMoney() {
        switch (gameFieldBet.getResult()) {
            case 1:
                return super.getBet();
            case 2:
                return 2*super.getBet();
            case 3:
                return 3*super.getBet();
            case -1:
                return -super.getBet();
            default:
                return 0;
        }
    }
///

    public void perform() {
        double depositMoney = 0;
        double betOnce = 0;
        // ask customer to make a deposit;
        System.out.println("make deposit first!");
        depositMoney = super.inputPositiveNumber();
        //finish deposit and check total residual
        System.out.println("after deposit, your total money is "+
                this.depositAccount(depositMoney));    
        //ask customer to input a bet
        System.out.println("make a bet now");
        do{betOnce = super.inputPositiveNumber();}
        while(!super.setBet(betOnce));
        

        gameFieldBet.GameRun();
        //check dice numbers
        System.out.println(gameFieldBet.toString());
        //change bankroll after one turn
        System.out.println("now you have "+
                super.changeAccount(this.changeMoney()));
       
    }
    /**
     * run field bet gamble
     * @param args 
     */
    public static void main(String[] args) {
        JRapsFieldBet play = new JRapsFieldBet();
        
        play.perform();
                
    }    
}
