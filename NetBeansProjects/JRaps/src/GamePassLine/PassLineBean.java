/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GamePassLine;

import MakingDice.RollingDie;

/**
 *
 * @author cha_xi
 */
public class PassLineBean extends RollingDie{
    private int point;

    public PassLineBean() {
       super();
        point = 11;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
    
}
