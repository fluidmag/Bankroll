/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GamePassLine;

import Constants.JRapsConstants;

/**
 * Details of Pass Line gamble
 * @author cha_xi
 */
public class PassLineImpl {

    // initialize passline and initial point=11 in default constructor!
    private int turns;
    // result: 1 means win, -1 means lose, 0 means continue
    private int result;
    private PassLineBean passLine = new PassLineBean();

    public PassLineImpl() {
        passLine = new PassLineBean();
        turns = 0;
        result = 0;
    }

    public int getResult() {
        return result;
    }

    public int getTurns() {
        return turns;
    }

    public PassLineBean getPassLine() {
        return passLine;
    }
/**
 * run PassLine gamble
 */
    public void GameRun() {
        // rolling dice
        passLine.rollingDice();
        //System.out.println("dice are: "+passLine.toString());
        //check result
        int i = passLine.sumTwoDice();
        switch (turns) {
            case 0:
                if (i == JRapsConstants.PassLineFirstTurnWinNum) {
                    result = 1;
                    break;
                } else if (i == passLine.getPoint()) {
                    result = 1;
                    break;
                } else {
                    result = 0;
                    for (int x : JRapsConstants.PassLineFirstTurnLoseNum) {
                        if (x == i) {
                            result = -1;
                            break;
                        }
                    }                  
                    passLine.setPoint(passLine.sumTwoDice());
                    break;
                }
            default:
                if (i == JRapsConstants.PassLineOtherTurnLoseNum) {
                    result = -1;
                } else if (i == passLine.getPoint()) {
                    result = 1;
                } else {
                    result = 0;
                }
            break;
        }
        
        
        turns++; 

    }

    @Override
    public String toString() {
        String str =  "PassLine: " + turns;
        switch(turns){
        case 1:
            str +="st turn, WinNum("+JRapsConstants.PassLineFirstTurnWinNum;
            str +=","+passLine.getPoint()+")";
            break;
        case 2: 
            str +="nd turn, WinNum("+ passLine.getPoint()+"), LoseNum(";
            str +=JRapsConstants.PassLineOtherTurnLoseNum+")";
            break;
        case 3:
            str +="rd turn, WinNum("+ passLine.getPoint()+"), LoseNum(";
            str +=JRapsConstants.PassLineOtherTurnLoseNum+")";
            break;
        default:
            str +="th turn, WinNum("+ passLine.getPoint()+"), LoseNum(";
            str +=JRapsConstants.PassLineOtherTurnLoseNum+")";
            break;
    }
        str += " dice"+passLine.toString()+" sum="+passLine.sumTwoDice();
        switch(result){
            case -1:
                str += " lose bet ";
                break;
            case 0:
                str += " continue ";
                break;
            case 1:
                str +=" win bet ";
                break;
            default:
                break;
        }
        return str;
    }
    
    
    
}
