/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GameAnySeven;

import Constants.JRapsConstants;

/**
 * Details of Any7 gamble
 * @author cha_xi
 */
public class AnySevenImpl {
     private int result;
     private AnySevenBean anySeven = new AnySevenBean();

    public AnySevenImpl() {
        anySeven = new AnySevenBean();
        result = 0;
    }

    public int getResult() {
        return result;
    }

 
    public AnySevenBean getPassLine() {
        return anySeven;
    }
/**
 * run PassLine gamble
 */
    public void GameRun() {
        // rolling dice
        anySeven.rollingDice();
        //System.out.println("dice are: "+passLine.toString());
        //check result
        int i = anySeven.sumTwoDice();
        result = 0;       
        
        if(i==JRapsConstants.AnySevenWinNum)
        {
            result = 4;
        }
        else{result = -1;}
       
        
    }

    @Override
    public String toString() {
        String str =  "Any7: ";        
        str += " dice"+anySeven.toString()+" sum="+anySeven.sumTwoDice();
        switch(result){
            case 4:
                str += " win quadruple bets ";
                break;
            case -1:
                str += " lose bet ";
                break;
            default:
                str +="Error, impossible!";
                break;
        }
        return str;
    }   
}
