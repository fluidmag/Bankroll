/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import Bankroll.Bankroll;

/**
 *
 * @author cha_xi
 */
public class MoneyTester {

    public MoneyTester() {
    }
    
    public void perform(){
        Bankroll bankroll1 = new Bankroll(100.0);
        System.out.println("Money in MoneyTester_1 is supposed to be 100");
        System.out.println("now, it is "+bankroll1.getFormattedMoney());
        
        double [] numbers = {1,2,3,4,5,6,7,8,9,10};
        System.out.println("now test add");
        for(double x:numbers ) {
            System.out.print(bankroll1.getFormattedMoney()+" add "+x+"=");
            bankroll1.add(x);
            System.out.println(bankroll1.getFormattedMoney());
        }
        
        System.out.println("now test substract");
        for(double x:numbers){
            System.out.print(bankroll1.getFormattedMoney()+" substract "+x+"="); 
            bankroll1.subtract(x);
            System.out.println(bankroll1.getFormattedMoney());
        }
    }
    
    
    public static void main(String[] args) {
        MoneyTester mt = new MoneyTester();
        mt.perform();
        System.exit(0);
    }
}
