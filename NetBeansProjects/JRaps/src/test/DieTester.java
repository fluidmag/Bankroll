/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import MakingDice.TheDie;

/**
 *
 * @author cha_xi
 */
public class DieTester {
    public static void main(String[] args) {
        DieTester dieTester = new DieTester();
        dieTester.perform();
        System.exit(0);
    }

    private void perform() {
        TheDie die = new TheDie();
        int [] numbers = new int[100];
        for(int i=0;i<100;i++){
            numbers[i] = i;
        }
        int [] frequency = new int[6];
        for(int i=0; i<6;i++){frequency[i] = 0;}
        for(int i:numbers){
            die.rollTheDie();
            frequency[die.getOneDie()-1]++;
            System.out.print(die.getOneDie()+" ");
        }
        System.out.println("Stats for the frequency of results:");
        for(int i=0;i<6;i++){
            System.out.println("frequency of "+(i+1)+" is "+frequency[i]/100.0);
        }
    }
    
}
