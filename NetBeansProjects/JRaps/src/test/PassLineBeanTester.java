/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import GamePassLine.PassLineBean;

/**
 *
 * @author cha_xi
 */
public class PassLineBeanTester {
    public static void main(String[] args) {
        PassLineBean passLine1 = new PassLineBean();
        for(int i=0;i<10;i++){
            passLine1.rollingDice();
            System.out.println(passLine1.toString()+" sum of two dice: "+
                    passLine1.sumTwoDice()+" point is "+passLine1.getPoint());
            passLine1.setPoint(passLine1.sumTwoDice());
        }
        
    }
}
