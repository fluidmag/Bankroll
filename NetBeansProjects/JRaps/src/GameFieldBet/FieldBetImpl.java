/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GameFieldBet;

import Constants.JRapsConstants;

/**
 *Details of Field Bet gamble
 * @author cha_xi
 */
public class FieldBetImpl {
    // result: 1 means win one bet, 2 means win double bet, 3 means win triple bet
    // -1 means lose bet
    private int result;
    private FieldBetBean fieldBet = new FieldBetBean();

    public FieldBetImpl() {
        fieldBet = new FieldBetBean();
        result = 0;
    }

    public int getResult() {
        return result;
    }

 
    public FieldBetBean getPassLine() {
        return fieldBet;
    }
/**
 * run PassLine gamble
 */
    public void GameRun() {
        // rolling dice
        fieldBet.rollingDice();
        //System.out.println("dice are: "+passLine.toString());
        //check result
        int i = fieldBet.sumTwoDice();
        result = 0;       
        
        for(int x:JRapsConstants.FieldBetLoseNum)
        {
            if(i==x){result = -1; return;}
        }
        for(int x:JRapsConstants.FieldBetWinNum)
        {
            if(i==x){result = 1;return;}
        }    

        if(i==JRapsConstants.FieldBetDoubleWinNum){result = 2; return;}
        
        if(i==JRapsConstants.FieldBetTripleWinNum){result = 3; return;}
        
    }

    @Override
    public String toString() {
        String str =  "FieldBet: ";        
        str += " dice"+fieldBet.toString()+" sum="+fieldBet.sumTwoDice();
        switch(result){
            case -1:
                str += " lose bet ";
                break;
            case 1:
                str += " win one bet ";
                break;
            case 2:
                str +=" win double bets ";
                break;
            case 3:
                str +=" win triple bets ";
                break;
            default:
                str +="Error, impossible!";
                break;
        }
        return str;
    }
       
}
